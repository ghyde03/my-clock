/**
 * 	========================================================
 * 	This is for the project to run tasks
 * 	current tasks: compile scss and browser sync
 *
 * 	from Terminal run gulp to get the project running
 *	{@link https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md}
 *	
 *  @author Gary Hyde 
 *  {@link http://garyhyde.com}
 *
 *  @version  0.1
 *
 *  The MIT License (MIT)
 *  
 *  Copyright (c) 2015 Gary Hyde
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  SOFTWARE.
 *  ========================================================
 */

// Gobals 
var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
 	//sass = require('gulp-sass'),
 	run = require('run-sequence'),
 	bs = require('browser-sync'),
 	prefixer = require('gulp-autoprefixer');

// default task will run in terminal if you only run gulp
gulp.task( 'default', function(){
	run(
	'styles',
	'watch'
	)
});

// use for gulp-sass incase gulp-ruby-sass is bad
// gulp.task( 'styles', function(){
// 	gulp.src('scss/main.scss')
// 		.pipe( 
// 			sass({
// 				outputStyle : 'nested'
// 			}) 
// 		)
// 		.pipe(
// 			prefixer({
// 				browsers : 'last 5 versions'
// 			})
// 		)
// 		.pipe( gulp.dest('css') )
// });

// task: gulp styles 
// compile scss to css
gulp.task( 'styles', function(){
	sass('scss/*.scss', { style : 'expanded', lineNumbers : true } )
		.pipe(
			//prefixer will prefix css styles automatically
			prefixer({
				browsers : 'last 5 versions'
			})
		) // compiles to the css folder
		.pipe( gulp.dest('css') )

});

// task: gulp watch
// this set up browsersync and will reload the browsers on file change for html, php, css
// proxy needs to be set to project path
gulp.task( 'watch', function(){
	bs.init({
		files : [
			'**/*.html',
			'**/*.php',
			'css/*.css'
		],
		proxy : 'http://localhost/frog/',
		open : false
	});
	gulp.watch('scss/**/*.scss', ['styles'] );
});

