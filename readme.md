# [Hyde-Starter](https://bitbucket.org/ghyde03/starter-template)

This is a simple project starting Template with HTML5 boilerplate and Gulp

## Table of contents

* [Quick start](#quick-start)
* [Creators](#creators)
* [Copyright and license](#copyright-and-license)


## Quick start

Several quick start options are available:

* [Download the latest release](https://bitbucket.org/ghyde03/starter-template/get/f1f717a5e7ba.zip).
* Clone the repo: `git clone git@bitbucket.org:ghyde03/starter-template.git`.


### What's included

Within the download you'll find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You'll see something like this:

```
starter/
├── css/
│   ├── main.css
├── js/
    ├── main.js
    └── plugins.js
```


## Bugs and feature requests

Have a bug or a feature request? Well... haven't made it that far yet.


## Creators

**Gary Hyde**

* <http://garyhyde.com>
* <https://bitbucket.org/ghyde03/>

**Richard Stovall**

* <http://codeydevelopment.com/>
* <https://bitbucket.org/pixelworlds/>


## Copyright and license

Code and documentation copyright 2015 Gary Hyde. Code released under [the MIT license](https://github.com/twbs/bootstrap/blob/master/LICENSE). 


